<?php 
session_start();
require_once "logica/Administrador.php";
require_once "logica/Producto.php";

$pid = "";
if(isset($_GET["pid"])){
    $pid = base64_decode($_GET["pid"]);    
}else{
    $_SESSION["id"]="";
    $_SESSION["rol"]="";
}
if(isset($_GET["cerrarSesion"]) || !isset($_SESSION["id"])){
    $_SESSION["id"]="";
}
?>
<html>
<head>
	<link  href="Style/design.css" rel="stylesheet">
	<script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>
	<link rel="icon" type="image/png" href="img/logo.png" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" >
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" ></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" ></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" ></script>
</head>

<body>


<?php 
	 	
		 $paginasSinSesion = array(
			 "presentacion/autenticar.php",
			 "presentacion/cliente/registrarCliente.php",
			 "presentacion/login.php",
		 );	
		 if($_SESSION["id"]!=""){
			 include "presentacion/menuAdministrador.php";
			 include $pid;
		 }else if(in_array($pid, $paginasSinSesion)){
			 include $pid;
		 }else{
			 include "presentacion/navegacion.php";			
			 include "presentacion/inicio.php";
		
		 } 
		 ?>

</body>

</html>