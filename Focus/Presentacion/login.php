

<link  href="Style/design.css" rel="stylesheet">
<?php
include "presentacion/navegacion.php";
?>
 <!--link de bootstrap-->
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
        crossorigin="anonymous">

		<div class="col-lg-3 col-md-4 col-12 ml-auto mr-auto text-center mt-5">
                        <div class="card ">
                            <div class="card-header text-white bg-dark">
                                <h4>Autenticacion</h4>
                            </div>
                              <div class="card-body">
                                <form action="index.php?pid=<?php echo base64_encode("presentacion/autenticar.php") ?>" method="post">
                                    <div class="form-group">
                                        <input name="correo" type="email" class="form-control" placeholder="Correo" required>
                                    </div>
                                    <div class="form-group">
                                        <input name="clave" type="password" class="form-control" placeholder="Contraseña" required>
                                    </div>
                                    <div class="form-group">
                                        <input name="ingresar" type="submit" class="form-control btn btn-dark" value="Ingresar">
                                    </div>
                                    <?php 
                                    if(isset($_GET["error"]) && $_GET["error"]==1){
                                        echo "<div class=\"alert alert-danger\" role=\"alert\">Error de correo o clave</div>";
                                   /*  }else if(isset($_GET["error"]) && $_GET["error"]==2){
                                        echo "<div class=\"alert alert-danger\" role=\"alert\">Su cuenta no ha sido activada</div>";    					   
                                    }else if(isset($_GET["error"]) && $_GET["error"]==3){
                                        echo "<div class=\"alert alert-danger\" role=\"alert\">Su cuenta ha sido inhabilitada</div>"; */
                                    }
                                    ?>
                                </form>
                                <p>Eres nuevo? <a href="index.php?pid=<?php echo base64_encode("presentacion/cliente/registrarCliente.php")?>">Registrate</a></p>
                            </div>
                        </div>
                    </div>


		
		