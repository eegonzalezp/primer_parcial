
<link  href="Style/design.css" rel="stylesheet">
<?php
$administrador = new Administrador($_SESSION["id"]);
$administrador->consultar();
?>
<nav class="navbar navbar-expand-lg ">
	<a class="navbar-brand text-dark"
		href="index.php?pid=<?php echo base64_encode("presentacion/sesionAdministrador.php") ?>">
		<i class="fas fa-home"></i></a>
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarSupportedContent"
		aria-controls="navbarSupportedContent" aria-expanded="false"
		aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item dropdown active"><a
				class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false">Juguete</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/producto/crearProducto.php") ?>">Insertar juguete</a>
					<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/producto/consultarProductoPagina.php") ?>">Consultar Pagina</a><a
						class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/producto/consultarProductoTodos.php") ?>">ConsultarTodos</a>
				</div></li>
			
		</ul>
		<ul class="navbar-nav">
			<li class="nav-item dropdown active"><a
				class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false"><strong>ADMIN</strong> <?php echo $administrador -> getNombre() ?> <?php echo $administrador -> getApellido() ?></a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/producto/crearProducto.php") ?>">Editar
						Perfil</a> <a class="dropdown-item" href="#">Actualizar Foto</a> <a
						class="dropdown-item" href="#">Cambiar contraseña</a>
				</div></li>
			<li class="nav-item active"><a class="nav-link"
				href="index.php?cerrarSesion=true"><strong>Cerrar Sesión</strong></a></li>
		</ul>
	</div>
</nav>