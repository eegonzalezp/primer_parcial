<nav class="navbar navbar-expand-lg ">
    <div class="container text-light">
  <a class="navbar-brand" href="index.php"><strong class="text-light"><i class="fab fa-foursquare"></i>ocus</strong></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
  <ion-icon name="menu-outline"></ion-icon>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto text-light">
      <li class="nav-item active ">
        <a class="nav-link" href="index.php">Inicio <span class="sr-only ">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Servicios</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Nosotros
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">¿Quienes somos?</a>
          <a class="dropdown-item" href="#">Misión</a>
          <a class="dropdown-item" href="#">Visión</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Contactanos</a>
        </div>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Mi cuenta
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/login.php")?>">Iniciar</a> 
          <a class="dropdown-item" href="#">Registrarme</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Recuperar mi cuenta</a>
        </div>
        
      </li>


     
    </ul>
   
  </div>
  </div>
</nav>
