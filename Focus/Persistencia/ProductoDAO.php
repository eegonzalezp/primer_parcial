<?php
class ProductoDAO{
    private $idProducto;
    private $nombre;
    private $cantidad;
    private $material;
       
    public function ProductoDAO($idProducto = "", $nombre = "", $cantidad = "", $material = ""){
        $this -> idProducto = $idProducto;
        $this -> nombre = $nombre;
        $this -> cantidad = $cantidad;
        $this -> material = $material;
    }
       
    public function insertar(){
        return "insert into Producto (nombre, cantidad, material)
                values ('" . $this -> nombre . "', '" . $this -> cantidad . "', '" . $this -> material . "')";
    }
    
    public function consultarTodos(){
        return "select idProducto, nombre, cantidad, material
                from Producto";
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        return "select idProducto, nombre, cantidad, material
                from producto 
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarCantidad(){
        return "select count(idProducto)
                from producto";
    }
    
}

?>